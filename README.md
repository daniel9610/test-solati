<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

## Installation
1. Copy the .env.example file to a file named .env, create a database and push your credentials in this file
2. execute the following commands in you'r terminal

- git clone https://gitlab.com/daniel9610/test-solati.git
- composer install
- php artisan:key generate
- php artisan migrate
- php artisan:passport install
- chmod -R 775 storage
- chmod -R 775 bootstrap/cache

3. Run your server with the following command

- php artisan serve

### Usage

In the first page, you see the login, register and see a simple dashboard.
After registering, you can use your credentials to login the next time

### Use the users end-point

- Authentication with passport

In this test, passport was used to authenticate the api, for authenticate, you use the following form in json format

url : localhost_or_your_url/api/login
type: Post
body{
    "email":"your email",
    "password":"your password"
}

"This return a Bearer Token"

If you want to see the list of registered users, use the following endpoint 

url : localhost_or_your_url/api/users
headers: 
"Authentication" : "Bearer your bearer token"  (example: Bearer asldfjñlasjglñkasjflkjasdjfasdf)

This must return the list of users in json format
 
